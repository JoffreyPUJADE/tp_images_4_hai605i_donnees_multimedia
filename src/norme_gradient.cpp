#include "Transformations.hpp"
#include <cmath>
#include <stdexcept>

int main(int argc, char** argv)
{
	int nH, nW, nTaille;

	if(argc != 3)
		throw std::runtime_error("Usage: ImageIn.pgm ImageOut.pgm");
	
	std::string nomImageLue = argv[1];
	std::string nomImageEcrite = argv[2];
	
	OCTET *ImgIn, *ImgOut;
	
	lire_nb_lignes_colonnes_image_pgm(nomImageLue.c_str(), &nH, &nW);
	
	nTaille = nH * nW;
	
	allocation_tableau(ImgIn, OCTET, nTaille);
	lire_image_pgm(nomImageLue.c_str(), ImgIn, nH * nW);
	allocation_tableau(ImgOut, OCTET, nTaille);
	
	for(int i=1;i<(nH-1);++i)
	{
		for(int j=1;j<(nW-1);++j)
		{
			long int vecteurHorizontal = (static_cast<long int>(ImgIn[i*nW+(j+1)]) - static_cast<long int>(ImgIn[i*nW+(j-1)]));
			long int vecteurVertical = (static_cast<long int>(ImgIn[(i+1)*nW+j]) - static_cast<long int>(ImgIn[(i-1)*nW+j]));
			
			long int x = vecteurHorizontal;
			long int y = vecteurVertical;
			
			long int norme = static_cast<long int>(sqrt((static_cast<long int>(pow(x, 2)) + static_cast<long int>(pow(y, 2)))));
			
			ImgOut[i*nW+j] = static_cast<OCTET>(norme);
		}
	}
	
	ecrire_image_pgm(nomImageEcrite.c_str(), ImgOut, nH, nW);
	
	free(ImgIn);
	free(ImgOut);
	
	return 0;
}
