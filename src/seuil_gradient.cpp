#include "Transformations.hpp"
#include <stdexcept>

int main(int argc, char** argv)
{
	int nH, nW, nTaille;

	if(argc != 4)
		throw std::runtime_error("Usage: ImageIn.pgm ImageOut.pgm Seuil");
	
	std::string nomImageLue = argv[1];
	std::string nomImageEcrite = argv[2];
	int S = type2Other<char*, int>(argv[3]);
	
	OCTET *ImgIn, *ImgOut;
	
	lire_nb_lignes_colonnes_image_pgm(nomImageLue.c_str(), &nH, &nW);
	
	nTaille = nH * nW;
	
	allocation_tableau(ImgIn, OCTET, nTaille);
	lire_image_pgm(nomImageLue.c_str(), ImgIn, nH * nW);
	allocation_tableau(ImgOut, OCTET, nTaille);
	
	for(int i=0;i<nH;++i)
	{
		for(int j=0;j<nW;++j)
		{
			long int vecteurHorizontal = (static_cast<long int>(ImgIn[i*nW+(j+1)]) - static_cast<long int>(ImgIn[i*nW+(j-1)]));
			long int vecteurVertical = (static_cast<long int>(ImgIn[(i+1)*nW+j]) - static_cast<long int>(ImgIn[(i-1)*nW+j]));
			
			long int x = vecteurHorizontal;
			long int y = vecteurVertical;
			
			long int norme = static_cast<long int>(sqrt((static_cast<long int>(pow(x, 2)) + static_cast<long int>(pow(y, 2)))));
			
			if(norme < S)
			{
				ImgOut[i*nW+j] = 0;
				
				/*if(ImgIn[i*nW+j - 1] < S)
					ImgOut[i*nW+j - 1] = 0;
				if(ImgIn[i*nW+j + 1] < S)
					ImgOut[i*nW+j + 1] = 0;*/
			}
			else
			{
				ImgOut[i*nW+j] = 255;
			}
		}
	}
	
	ecrire_image_pgm(nomImageEcrite.c_str(), ImgOut, nH, nW);
	
	free(ImgIn);
	free(ImgOut);
	
	return 0;
}
